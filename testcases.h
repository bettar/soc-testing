/* 
 * File:   testcases.h
 * Author: Alessandro Bettarini
 *
 * Created on 1 June 2018, 10:18 AM
 */

#ifndef TESTCASES_H
#define TESTCASES_H

#ifdef __cplusplus
extern "C" {
#endif

void test_module(volatile struct st_module *m);

#ifdef __cplusplus
}
#endif

#endif /* TESTCASES_H */

