/* 
 * File:   testcases.h
 * Author: Alessandro Bettarini
 *
 * Created on 1 June 2018, 10:18 AM
 */

#include "iodefine.h"
#include "registers.h"
#include "logging.h"
#include "testcases.h"

/* Knowing that errors are detected when a component is enabled,
 * we enable them in different ways, each time checking for errors
 * 1) Enable component 0
 * 2) Enable component 1
 * 3) Enable component 0 then enable component 1
 * 4) Enable component 1 then enable component 0 (maybe redundant)
 */
void test_module(volatile struct st_module *m)
{
    unsigned char errorCode;

    log_info("Enable module");
    unsigned char revision = reg_readRevision(m);
    if (revision == SOC_VER_12)
        reg_writeModuleStatus(m, MODULE_ENABLE_V12);
    else if (revision == SOC_VER_20)
        reg_writeModuleStatus(m, MODULE_ENABLE_V20);
    else {
        log_warning("Unknown module version");
        return;
    }

    // 1)
    log_info("Enable component 0");        
    reg_writeComponentStatus0(m, COMPONENT_ENABLE);
    if (reg_checkComponentError0(m)) {
        errorCode = reg_readComponentError0(m);
        log_componentError(m, 0, errorCode);
    }
    log_info("Disable component 0");        
    reg_writeComponentStatus0(m, COMPONENT_DISABLE);

    // 2)
    log_info("Enable component 1");        
    reg_writeComponentStatus1(m, COMPONENT_ENABLE);
    if (reg_checkComponentError1(m)) {
        errorCode = reg_readComponentError1(m);
        log_componentError(m, 1, errorCode);
    }
    log_info("Disable component 1");        
    reg_writeComponentStatus1(m, COMPONENT_DISABLE);

    // 3)
    log_info("Enable components 0 and 1");        
    reg_writeComponentStatus0(m, COMPONENT_ENABLE);
    reg_writeComponentStatus1(m, COMPONENT_ENABLE);
    if (reg_checkComponentError0(m)) {
        errorCode = reg_readComponentError0(m);
        log_componentError(m, 0, errorCode);
    }
    if (reg_checkComponentError1(m)) {
        errorCode = reg_readComponentError1(m);
        log_componentError(m, 1, errorCode);
    }
    log_info("Disable components 0 and 1");        
    reg_writeComponentStatus0(m, COMPONENT_DISABLE);
    reg_writeComponentStatus1(m, COMPONENT_DISABLE);

    // 4)
    log_info("Enable components 1 and 0");        
    reg_writeComponentStatus1(m, COMPONENT_ENABLE);
    reg_writeComponentStatus0(m, COMPONENT_ENABLE);
    if (reg_checkComponentError1(m)) {
        errorCode = reg_readComponentError1(m);
        log_componentError(m, 1, errorCode);
    }
    if (reg_checkComponentError0(m)) {
        errorCode = reg_readComponentError0(m);
        log_componentError(m, 0, errorCode);
    }
    log_info("Disable components 1 and 0");        
    reg_writeComponentStatus1(m, COMPONENT_DISABLE);
    reg_writeComponentStatus0(m, COMPONENT_DISABLE);

    //
    log_info("Disable module");   
    reg_writeModuleStatus(m, MODULE_DISABLE);
}
