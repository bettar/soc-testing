/* Alessandro Bettarini - 31 May 2018
 * 
 * PURPOSE:
 *  The purpose of this module is to provide an "abstraction layer"
 *  so that the test case code can be developed in a register agnostic manner.
 */

#include <stdio.h>
#include "registers.h"

void reg_show(volatile struct st_module *m, int line)
{
#if 0
    printf("Module status: %p\n", &m->STATUS_REG);            
    printf("Components status: %p\n", &m->COMP_STATUS_REG);            
    printf("ERROR_REG: %p\n", &m->ERROR_REG);            
    printf("REV_REG:%p\n", &m->REV_REG);            
    printf("ID_REG: %p\n", &m->ID_REG);     
#endif

#ifndef REAL_SOC
    printf("Module ID: %d, version %d.%d\n",
            m->ID_REG,
            m->REV_REG.BIT.MAJOR,
            m->REV_REG.BIT.MINOR);    
    printf("Status registers: 0x%02x (module), 0x%02x (components)\n",
            m->STATUS_REG,
            m->COMP_STATUS_REG.BYTE);               
#endif
}

unsigned char reg_readModuleStatus(volatile struct st_module *m)
{
    return m->STATUS_REG;
}

void reg_writeModuleStatus(volatile struct st_module *m, unsigned char s)
{
    m->STATUS_REG = s;
}

unsigned char reg_readComponentStatus(volatile struct st_module *m)
{
    return m->COMP_STATUS_REG.BYTE;
}

void reg_writeComponentStatus0(volatile struct st_module *m, unsigned char s)
{
    m->COMP_STATUS_REG.BIT.ENA0 = s;

#ifndef REAL_SOC
    // Simulate some error code
    m->ERROR_REG.BIT.ERR0 = 1;
    m->ERROR_REG.BIT.CODE0 = 5; // TODO: use random number
#endif
}

void reg_writeComponentStatus1(volatile struct st_module *m, unsigned char s)
{
    m->COMP_STATUS_REG.BIT.ENA1 = s;

#ifndef REAL_SOC
    // Simulate some error code
    m->ERROR_REG.BIT.ERR1 = 1;
    m->ERROR_REG.BIT.CODE1 = 6; // TODO: use random number
#endif
}

bool reg_checkComponentError0(volatile struct st_module *m)
{
    if (m->ERROR_REG.BIT.ERR0)
        return true;

    return false;
}

bool reg_checkComponentError1(volatile struct st_module *m)
{
    if (m->ERROR_REG.BIT.ERR1)
        return true;

    return false;    
}

unsigned char reg_readComponentError0(volatile struct st_module *m)
{
    return m->ERROR_REG.BIT.CODE0;
}

unsigned char reg_readComponentError1(volatile struct st_module *m)
{
    return m->ERROR_REG.BIT.CODE1;    
}

unsigned char reg_readRevision(volatile struct st_module *m)
{
    return m->REV_REG.BYTE;
}
