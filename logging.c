/* 
 * File:   logging.h
 * Author: Alessandro Bettarini
 *
 * Created on 1 June 2018, 9:52 AM
 * 
 * Simple logging to console
 */

#include <stdio.h>

#include "registers.h"
#include "logging.h"

static int logLevel = LOG_LEVEL_ERROR;

void log_setLevel(int level)
{
    logLevel = level;
}

void log_componentError(volatile struct st_module *module,
                        int component,
                        unsigned char errorCode)
{
    if (logLevel >= LOG_LEVEL_ERROR) {
        printf("ERROR: ");   
        reg_show(module, __LINE__);
        printf("Component %d, error code %d\n",
                component,
                (unsigned int)errorCode);
    }
}

void log_warning(char *m)
{
    if (logLevel >= LOG_LEVEL_WARNING)
        printf("WARNING: %s\n", m);   
}

void log_info(char *m)
{
    if (logLevel >= LOG_LEVEL_INFO)
        printf("INFO: %s\n", m);   
}

void log_debug(char *m)
{
    if (logLevel >= LOG_LEVEL_DEBUG)
        printf("DEBUG: %s\n", m);   
}