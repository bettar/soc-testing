/* 
 * File:   registers.h
 * Author: Alessandro Bettarini
 *
 * Created on 31 May 2018, 2:18 PM
 */

#ifndef REGISTERS_H
#define REGISTERS_H

#include <stdbool.h>
#include "iodefine.h"

#ifdef __cplusplus
extern "C" {
#endif

void reg_show(volatile struct st_module *m, int line);

unsigned char reg_readModuleStatus(volatile struct st_module *m);
void reg_writeModuleStatus(volatile struct st_module *m, unsigned char s);

unsigned char reg_readComponentStatus(volatile struct st_module *m);
void reg_writeComponentStatus0(volatile struct st_module *m, unsigned char s);
void reg_writeComponentStatus1(volatile struct st_module *m, unsigned char s);

bool reg_checkComponentError0(volatile struct st_module *m);
bool reg_checkComponentError1(volatile struct st_module *m);
unsigned char reg_readComponentError0(volatile struct st_module *m);
unsigned char reg_readComponentError1(volatile struct st_module *m);

unsigned char reg_readRevision(volatile struct st_module *m);

#ifdef __cplusplus
}
#endif

#endif /* REGISTERS_H */

