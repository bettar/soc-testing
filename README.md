# SoC testing

## C language assignment

### Purpose

- The stated purpose of this assignment was to evaluate the C-language competence of a job applicant at a major semiconductor company. The company initially emailed to the candidate a text document describing the "mission". Such document cannot (yet) be published here because of a copyright header in the file.

- Pending authorisation from the company to publish the original assignment description, I will state in my own words the original task to be accomplished:

    - Provide a set of C language functions to test for errors in a SoC (System on Chip)
    - The SoC has 2 identical modules, memory mapped
    - Each module has two identical components
    - The SoC has a set of 5 internal registers to control the chip
    - A detailed description of all the bits in each register was provided.
    - There is an error register for each component, whose content is valid and can be read when the component is turned on.
    - There is concern that the components might interact with each other

### Time frame and requirements
- The candidate was given 48 hours to complete the assignment
- Full freedom was given to the candidate to make any assumptions on the requirements, requesting only that such assumption be documented.

### Final hiring decision
- After submitting the completed assignment in time, the company acknowledged it by saying:

    "We will peer review the assignment with the senior engineers in the team during this week and I will get back to you before the end of the week."

- The entire week passed and three more days of the following week, without the company notifying the candidate of any result.

- On the following Wednesday the candidate approached the company, and eventually being dismissed by saying: "Unfortunately the assignment we received did not meet the quality level that we would like to see for this role."

- This is the last the candidate heard from the company.