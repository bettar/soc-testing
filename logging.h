/* 
 * File:   logging.h
 * Author: Alessandro Bettarini
 *
 * Created on 1 June 2018, 9:52 AM
 */

#ifndef LOGGING_H
#define LOGGING_H

#include "iodefine.h"

#ifdef __cplusplus
extern "C" {
#endif

enum {
    LOG_LEVEL_NONE = 0, // silent
    LOG_LEVEL_FATAL,
    LOG_LEVEL_ERROR,    // default
    LOG_LEVEL_WARNING,
    LOG_LEVEL_INFO,
    LOG_LEVEL_DEBUG,
    LOG_LEVEL_TRACE     // verbose
};

void log_setLevel(int level);

void log_componentError(volatile struct st_module *module,
                        int component,
                        unsigned char errorCode);

void log_warning(char *m);
void log_info(char *m);
void log_debug(char *m);

#ifdef __cplusplus
}
#endif

#endif /* LOGGING_H */

