/* Alessandro Bettarini - 31 May 2018
 * 
 * NOTE: the big endian case has not been tested yet.
 */

#ifndef _DEFINE_SOC_H_
#define _DEFINE_SOC_H_

#include <asm/byteorder.h>

// STATUS_REG
#define MODULE_DISABLE      0
#define MODULE_ENABLE_V12   1
#define MODULE_ENABLE_V20   0x80

// COMP_STATUS_REG
#define COMPONENT_DISABLE   1
#define COMPONENT_ENABLE    2

// REV_REG
#define SOC_VER_12          0x12
#define SOC_VER_20          0x20

/************************************************************************/
/*      SOC Include File                                     Ver 1.2    */
/************************************************************************/
struct st_module {                                      /* struct module */
              unsigned char STATUS_REG;                 /* STATUS_REG   */
              unsigned char unused1[3];                 /*              */
              union {                                   /* COMP_STATUS_REG */
                    unsigned char BYTE;                 /*  Byte Access */
                    struct {                            /*  Bit  Access */
#if defined(__LITTLE_ENDIAN_BITFIELD)
                           unsigned char ENA0:4;        /*    Enable    */
                           unsigned char ENA1:4;        /*    Enable    */
#else
                           unsigned char ENA1:4;        /*    Enable    */
                           unsigned char ENA0:4;        /*    Enable    */
#endif
                           }      BIT;                  /*              */
                    } COMP_STATUS_REG;                  /*              */
              unsigned char unused2[3];                 /*              */
              union {                                   /* ERROR_REG    */
                    unsigned char BYTE;                 /*  Byte Access */
                    struct {                            /*  Bit  Access */
#if defined(__LITTLE_ENDIAN_BITFIELD)
                           unsigned char ERR0:1;        /*    ERR0      */
                           unsigned char CODE0:3;       /*    CODE0     */
                           unsigned char ERR1:1;        /*    ERR1      */
                           unsigned char CODE1:3;       /*    CODE1     */
#else
                           unsigned char CODE1:3;       /*    CODE1     */
                           unsigned char ERR1:1;        /*    ERR1      */
                           unsigned char CODE0:3;       /*    CODE0     */
                           unsigned char ERR0:1;        /*    ERR0      */
#endif
                           }      BIT;                  /*              */
                    } ERROR_REG;                        /*              */
              unsigned char unused3[0xef];              /*              */
              union {                                   /* REV_REG      */
                    unsigned char BYTE;                 /*  Byte Access */
                    struct {                            /*  Bit  Access */
#if defined(__LITTLE_ENDIAN_BITFIELD)
                           unsigned char MINOR:4;       /*    MINOR     */
                           unsigned char MAJOR:4;       /*    MAJOR     */
#else
                           unsigned char MAJOR:4;       /*    MAJOR     */
                           unsigned char MINOR:4;       /*    MINOR     */
#endif
                           }      BIT;                  /*              */
                    } REV_REG;                          /*              */
              unsigned char unused4[3];                 /*              */
              unsigned char ID_REG;                     /*              */
};                                                      /*              */

#define NUM_MODULES     3
#define MODULE2         (*(volatile struct  st_module *)0x010200)
#define MODULE1         (*(volatile struct  st_module *)0x010100)
#define MODULE0         (*(volatile struct  st_module *)0x010000)

#endif
