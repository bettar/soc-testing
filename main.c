/* AUTHOR:
 *  Alessandro Bettarini
 *
 * DATE:
 *  31 May 2018
 * 
 * REFERENCE:
 *  home_assignment_v1.6_2017.06.14.text
 * 
 * DESCRIPTION:
 *  The implementation has been intentionally kept simple
 *  and self-contained, that is, without resorting to third party
 *  libraries and frameworks (Googletest, log4c, Doxygen, etc.)
 * 
 * ASSUMPTIONS:
 * - C standard should be at least C99, because of the 'bool' data type usage.
 * - No high level approach (ex: cmake) has been used to create
 *   a project makefile.
 * - No version revision control (ex: git) has been setup.
 * - No specific coding style has been adopted. As a result, some apparent
 *   inconsistencies in style and formatting (ex: headers) might be observed
 *   and should be allowed.
 * - No build script or makefile has been provided because they would be
 *   specific to a platform and tool-chain, and there was no such
 *   requirement in the assignment.
 * 
 * IMPLEMENTATION:
 *  "Hosted" environment (PC)
 *      - The SoC is simulated on the PC. This is how the code has been
 *        developed and tested, specifically on Linux with NetBeans IDE
 *        and GCC 7.3.0
 *      - Different compilers might produce more strict warnings or even errors
 *        which have not yet been checked.
 * 
 *  "Hosted" environment (target system: with RTOS)
 *      - This code has never been integrated in any RTOS environment
 * 
 *  "Free standing" environment (target system: bare metal)
 *      - This code has never been compiled with an ARM toolkit, therefore
 *        a linker file has not been created
 * 
 * LICENSE:
 *  This code is distributed under license "GNU General Public License v3.0"
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <asm/byteorder.h>   // for endianness

#include "iodefine.h"
#include "registers.h"
#include "logging.h"
#include "testcases.h"

#ifdef __arm__
/* When REAL_SOC is not defined
 * the SoC is simulated (memory mapped in the PC RAM (heap)).
 * 
 * NOTE: for verification, it can also be compiled on PC with REAL_SOC defined.
 * IMPORTANT: don't run on PC if compiled with REAL_SOC forcibly defined !
 */
#define REAL_SOC
#endif

int main(int argc, char**argv)
{
    log_setLevel(LOG_LEVEL_ERROR);  // TODO: allow setting from command line

#if defined(__LITTLE_ENDIAN_BITFIELD)
    log_debug("LITTLE_ENDIAN_BITFIELD");
#elif defined (__BIG_ENDIAN_BITFIELD)
    log_debug("BIG_ENDIAN_BITFIELD");
#endif

#ifdef REAL_SOC
    volatile struct st_module *m[] = {&MODULE0, &MODULE1, &MODULE2};

    // Make sure all modules are off before starting any test
    for (int i = 0; i < NUM_MODULES; i++)
        reg_writeModuleStatus(m[i], MODULE_DISABLE);
#else
    volatile struct st_module m0;
    volatile struct st_module m1;
    volatile struct st_module m2;
    volatile struct st_module *m[] = {&m0, &m1, &m2};
    
    // Initialize to default values like the real chip
    for (int i = 0; i < NUM_MODULES; i++) {
        m[i]->STATUS_REG = MODULE_DISABLE;
        m[i]->COMP_STATUS_REG.BIT.ENA0 = COMPONENT_DISABLE;
        m[i]->COMP_STATUS_REG.BIT.ENA1 = COMPONENT_DISABLE;
        // The following registers are read-only on the real chip
        m[i]->ERROR_REG.BYTE = 0;
        m[i]->REV_REG.BYTE = SOC_VER_12;
        m[i]->ID_REG = i;
    }

    // Part 2 - one module is a different version
    m[0]->REV_REG.BYTE = SOC_VER_20;
#endif

    // Test case 1 - test one module at a time for component errors
    for (int i = 0; i < NUM_MODULES; i++) {
        printf("*** *** Testing module %i *** ***\n", i);        
        test_module(m[i]);
    }    

    return 0;
}
